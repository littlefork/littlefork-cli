import winston from "winston";

// Setup logging
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
  timestamp: true,
  colorize: true,
  level: "debug",
});

export const info = winston.info;
export const warn = winston.warn;
export const error = winston.error;
export const debug = winston.debug;

export default {info, warn, error, debug};
